import { Command } from 'commander';
import colors from 'colors';

import packageJson from '../package.json';
import { createDuck } from './actions';
import { enhance } from './prompts';

const program = new Command();

program.storeOptionsAsProperties(false).passCommandToAction(false);

program.version(
  packageJson.version,
  '-v, --version',
  'Output the current version',
);

program
  .command('enhance', { isDefault: true })
  .option('--update', 'Updates all config and other template files')
  .action(async (options) => {
    try {
      await enhance({ options });

      console.log(
        `${colors.green('✔')} Successfuly generated configuration files.`,
      );
    } catch (error) {
      console.log(`${colors.red('✖')} ${error.message}`);
      process.exit(1);
    }
  });

program
  .command('create-duck <duck>')
  .description("Creates a folder with a `duck` pattern and it's file contents")
  .option('--name <name>', `Override slice's name`)
  .action(async (duck, options) => {
    try {
      const { message } = await createDuck({ duck, options });

      console.log(`${colors.green('✔')} Successfuly created duck files.`);
      console.log(message);
    } catch (error) {
      console.log(`${colors.red('✖')} ${error.message}`);
      process.exit(1);
    }
  });

program.parse();
