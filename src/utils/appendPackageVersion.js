const appendPackageVersion = ({ name, version }) =>
  [name, version].filter(Boolean).join('@');

export default appendPackageVersion;
