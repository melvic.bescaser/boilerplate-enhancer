import colors from 'colors';

const beautifyPackages = ({ packages, color = colors.blue }) => {
  const { length } = packages;

  return packages.reduce(
    (prev, curr, i) =>
      `${prev}${(i > 0 && (i === length - 1 ? ', and ' : ', ')) || ''}${color(
        curr,
      )}`,
    '',
  );
};

export default beautifyPackages;
