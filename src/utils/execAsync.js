import shell from 'shelljs';

const execAsync = ({ command, options = { silent: true } }) =>
  new Promise((resolve, reject) =>
    shell.exec(command, options, (err) => {
      if (err) {
        return reject();
      }

      return resolve();
    }),
  );

export default execAsync;
