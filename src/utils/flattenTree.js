const flattenTree = ({ tree, test = () => true }) =>
  tree.reduce((prev, curr) => {
    if (!test(curr)) {
      return prev;
    }

    const { name, version, children } = curr;

    if (!(children && children.length > 0)) {
      return [...prev, { name, version }];
    }

    return [
      ...prev,
      { name, version },
      ...flattenTree({ tree: children, test }),
    ];
  }, []);

export default flattenTree;
