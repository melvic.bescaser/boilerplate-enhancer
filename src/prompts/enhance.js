import colors from 'colors';
import { createPromptModule, Separator } from 'inquirer';

import { ProjectType } from '../config/constants';
import { getPackages } from '../services';
import { enhance as enhanceAction } from '../actions';

const prompt = createPromptModule();

const enhance = async ({ options }) => {
  const questions = [
    {
      type: 'list',
      name: 'projectType',
      message: 'What tyoe of project will you be working on?',
      choices: () => {
        const notSupportedProjecTypes = [ProjectType.Node, ProjectType.Express];

        return Object.keys(ProjectType).map((key) => ({
          value: ProjectType[key],
          name: key,
          disabled: notSupportedProjecTypes.includes(ProjectType[key])
            ? 'Currently not supported'
            : false,
        }));
      },
    },
    {
      when: ({ projectType }) => projectType === ProjectType.React,
      type: 'confirm',
      name: 'isUsingReactScripts',
      message: `Is the project uses ${colors.cyan(
        'react-scripts',
      )} from ${colors.blue('create-react-app')} library?`,
      default: false,
    },
    {
      when: ({ projectType, isUsingReactScripts }) =>
        projectType === ProjectType.React && !isUsingReactScripts,
      type: 'confirm',
      name: 'isUsingSingleSpa',
      message: `Is the project uses ${colors.blue('single-spa')} library?`,
      default: true,
    },
    {
      when: ({ projectType, isUsingSingleSpa }) =>
        projectType === ProjectType.React && isUsingSingleSpa,
      type: 'input',
      name: 'orgName',
      message: `Please input the ${colors.cyan('orgName')} from ${colors.blue(
        'webpack.config.js',
      )} file.`,
      validate: (input) => {
        if (input) {
          return true;
        }

        return `Property ${colors.cyan('orgName')} is required!`;
      },
    },
    {
      when: ({ projectType, isUsingSingleSpa }) =>
        projectType === ProjectType.React && isUsingSingleSpa,
      type: 'input',
      name: 'projectName',
      message: `Please input the ${colors.cyan(
        'projectName',
      )} from ${colors.blue('webpack.config.js')} file.`,
      validate: (input) => {
        if (input) {
          return true;
        }

        return `Property ${colors.cyan('projectName')} is required!`;
      },
    },
    {
      type: 'checkbox',
      name: 'packages',
      message: 'What packages would you like to install?',
      choices: (answers) => {
        const { dependencies, devDependencies } = getPackages(answers);
        let choices = [];

        if (dependencies.length > 0) {
          choices = [
            ...choices,
            new Separator('--- Dependencies ---'),
            ...dependencies,
          ];
        }

        if (devDependencies.length > 0) {
          choices = [
            ...choices,
            new Separator('--- Dev Dependencies ---'),
            ...devDependencies,
          ];
        }

        return choices;
      },
      default: (answers) => {
        const { dependencies, devDependencies } = getPackages(answers);

        return [...dependencies, ...devDependencies];
      },
    },
  ];

  const answers = await prompt(questions);

  await enhanceAction({ answers, options });
};

export default enhance;
