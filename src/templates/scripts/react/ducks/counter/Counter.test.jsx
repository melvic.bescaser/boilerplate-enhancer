import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { connect, Provider } from 'react-redux';
import configureStore from '../configureStore';
import {
  selectors as counterSelectors,
  actions as counterActions,
} from './index';

// eslint-disable-next-line react/prop-types
const Counter = ({ value, fetchValue, doubledValue, increment, decrement }) => {
  return (
    <div>
      <span data-testid="value">{value}</span>
      <span data-testid="value-selector">{doubledValue}</span>
      <button type="button" onClick={() => increment()} data-testid="increment">
        increment
      </button>
      <button type="button" onClick={() => decrement()} data-testid="decrement">
        decrement
      </button>
      <button
        type="button"
        onClick={() => fetchValue()}
        data-testid="fetchValue"
      >
        fetchValue
      </button>
    </div>
  );
};

const ConnectedCounter = connect(
  (state) => ({
    value: state.counter.value,
    doubledValue: counterSelectors.doubledValue(state.counter),
  }),
  {
    increment: counterActions.increment,
    decrement: counterActions.decrement,
    fetchValue: counterActions.fetchValue,
  },
)(Counter);

const store = configureStore();

const TestComponent = () => (
  <Provider store={store}>
    <ConnectedCounter />
  </Provider>
);

describe('Redux Integration Test', () => {
  it("'s store should be read", () => {
    const { getByTestId } = render(<TestComponent />);

    expect(getByTestId('value').textContent).toBe('1');
    expect(getByTestId('value-selector').textContent).toBe('2');
  });

  it("'s actions works", () => {
    const { getByTestId } = render(<TestComponent />);

    fireEvent.click(getByTestId('increment'));

    expect(getByTestId('value').textContent).toBe('2');
    expect(getByTestId('value-selector').textContent).toBe('4');
    fireEvent.click(getByTestId('decrement'));

    expect(getByTestId('value').textContent).toBe('1');
    expect(getByTestId('value-selector').textContent).toBe('2');
  });

  it("'s saga works", async () => {
    const { getByTestId } = render(<TestComponent />);

    fireEvent.click(getByTestId('fetchValue'));

    await waitFor(() => {
      expect(getByTestId('value').textContent).toBe('123');
      expect(getByTestId('value-selector').textContent).toBe('246');
    });
  });
});
