import { put, takeLatest, delay } from 'redux-saga/effects';
import { actions } from './slice';

function* fetchValueSaga() {
  try {
    const value = 123;
    yield delay(500);

    yield put({ type: actions.fetchValueFulfilled.type, value });
  } catch (e) {
    yield put({ type: actions.fetchValueRejected.type });
  }
}

function* valueWatcher() {
  yield takeLatest(actions.fetchValue.type, fetchValueSaga);
}

export default {
  valueWatcher,
};
