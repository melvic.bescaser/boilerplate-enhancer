import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: 1,
  status: 'idle',
};

const slice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    fetchValue: (state) => {
      state.status = 'loading';
    },
    fetchValueFulfilled: (state, { value }) => {
      state.status = 'idle';
      state.value = value;
    },
    fetchValueRejected: (state) => {
      state.status = 'idle';
    },
  },
  extraReducers: {},
});

export default slice;

export const { reducer, actions } = slice;
