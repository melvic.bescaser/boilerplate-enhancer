const doubledValue = (state) => state.value * 2;

export default {
  doubledValue,
};
