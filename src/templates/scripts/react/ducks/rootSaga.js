import { all, fork } from 'redux-saga/effects';

import { sagas as counter } from './counter';

const sagas = [counter];

export default function* rootSaga() {
  yield all(
    sagas.reduce(
      (sagasSoFar, saga) => [
        ...sagasSoFar,
        ...Object.values(saga).map((effect) => fork(effect)),
      ],
      [],
    ),
  );
}
