import { combineReducers } from '@reduxjs/toolkit';

import { reducer as counter } from './counter';

const store = combineReducers({
  counter,
});

export default store;
