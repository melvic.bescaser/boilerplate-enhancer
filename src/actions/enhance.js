import { ProjectType } from '../config/constants';
import { composeAsync } from '../utils';
import {
  installPackages,
  addPackageJsonConfig,
  clearConfigs,
  createEslintConfig,
  createPrettierConfig,
  createBabelConfig,
  createJestConfig,
  createJsConfig,
  createWebpackConfig,
  setupRedux,
} from '../services';

const enhance = async ({ answers, options }) => {
  let actions = [];

  if (answers.projectType === ProjectType.React) {
    if (options.update) {
      if (!answers.isUsingReactScripts) {
        actions = [createWebpackConfig, createBabelConfig];
      }

      actions = [
        ...actions,
        createJsConfig,
        createJestConfig,
        createPrettierConfig,
        createEslintConfig,
        addPackageJsonConfig,
        clearConfigs,
        installPackages,
      ];
    } else {
      actions = [setupRedux];

      if (!answers.isUsingReactScripts) {
        actions = [...actions, createWebpackConfig, createBabelConfig];
      }

      actions = [
        ...actions,
        createJsConfig,
        createJestConfig,
        createPrettierConfig,
        createEslintConfig,
        clearConfigs,
        addPackageJsonConfig,
        installPackages,
      ];
    }
  }

  if (actions.length === 0) {
    throw new Error(`'Failed operation, empty action(s) to invoke`);
  }

  await composeAsync(...actions)({ answers, options });
};

export default enhance;
