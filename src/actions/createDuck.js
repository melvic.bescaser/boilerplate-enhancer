import path from 'path';

import colors from 'colors';
import fse from 'fs-extra';
import ora from 'ora';
import tree from 'tree-node-cli';
import dedent from 'dedent';
import camelCase from 'lodash.camelcase';
import getIn from 'lodash.get';
import handlebars from 'handlebars';

import { Dependencies } from '../config/constants';
import { flattenTree, beautifyPackages } from '../utils';

const createDuck = async ({ duck, options }) => {
  const spinner = ora();

  spinner.start();

  const packageJson = await fse.readJson(
    path.resolve(process.cwd(), 'package.json'),
  );
  const requiredPackages = flattenTree({
    tree: Dependencies.filter(({ name }) => name === 'redux'),
  }).map(({ name }) => name);

  if (
    !requiredPackages.every((requiredPackage) =>
      getIn(packageJson, `dependencies.${requiredPackage}`),
    )
  ) {
    spinner.stop();

    const beautifiedPackages = beautifyPackages({
      packages: requiredPackages,
      color: colors.cyan,
    });

    throw new Error(
      `Failed operation, package(s) ${beautifiedPackages} does not exists!`,
    );
  }

  const duckDirectory = path.resolve(process.cwd(), 'src/ducks', duck);
  const isDirectoryExists = await fse.pathExists(duckDirectory);

  if (isDirectoryExists) {
    spinner.stop();

    throw new Error(
      `Failed operation, duck folder ${colors.cyan(duck)} is exists!`,
    );
  }

  const emptyExportDefaultObjectTemplate = 'export default {}';
  const indexTemplate = dedent(`
    export { default, reducer, actions } from './slice';
    export { default as selectors } from './selectors';
    export { default as sagas } from './sagas';
  `);
  let sliceTemplate;

  try {
    sliceTemplate = handlebars.compile(
      (
        await fse.readFile(
          path.resolve(__dirname, '../templates/dynamic/react/duck/slice.hbs'),
        )
      ).toString(),
    );
  } catch (error) {
    spinner.stop();

    throw new Error(
      `Failed operation, unable to read duck's ${colors.cyan(
        'slice',
      )} template.`,
    );
  }

  const files = [
    {
      name: 'slice.js',
      data: sliceTemplate({
        name: typeof options.name === 'string' ? options.name : camelCase(duck),
      }),
    },
    {
      name: 'sagas.js',
      data: emptyExportDefaultObjectTemplate,
    },
    {
      name: 'selectors.js',
      data: emptyExportDefaultObjectTemplate,
    },
    {
      name: 'index.js',
      data: indexTemplate,
    },
  ];

  try {
    await Promise.all(
      files.map(({ name, data }) =>
        fse.outputFile(path.resolve(duckDirectory, name), data),
      ),
    );

    spinner.stop();

    return {
      message: `src/ducks/${tree(duckDirectory, { trailingSlash: true })}`,
    };
  } catch {
    spinner.stop();

    throw new Error(`Failed operation, writing duck files didn't complete.`);
  }
};

export default createDuck;
