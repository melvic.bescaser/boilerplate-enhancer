export const ProjectType = {
  Node: 'node',
  Express: 'express',
  React: 'react',
};

export const AbsoluteImports = {
  Src: { name: '@src', path: 'src' },
  Mocks: { name: '@mocks', path: 'src/__mocks__' },
  Assets: { name: '@assets', path: 'src/assets' },
  Commons: { name: '@commons', path: 'src/commons' },
  Components: { name: '@components', path: 'src/components' },
  Ducks: { name: '@ducks', path: 'src/ducks' },
  Hooks: { name: '@hooks', path: 'src/hooks' },
  Layouts: { name: '@layouts', path: 'src/layouts' },
  Views: { name: '@views', path: 'src/views' },
};

export const Dependencies = [
  { name: 'react-router-dom', types: [ProjectType.React] },
  {
    name: 'redux',
    types: [ProjectType.React],
    children: [
      { name: 'react-redux', types: [ProjectType.React] },
      { name: '@reduxjs/toolkit', types: [ProjectType.React] },
      { name: 'redux-saga', types: [ProjectType.React] },
    ],
  },
  { name: 'prop-types', types: [ProjectType.React] },
];

export const DevDependencies = [
  { name: 'file-loader', types: [ProjectType.React], isForEjected: true },
  {
    name: 'babel-plugin-module-resolver',
    types: [ProjectType.React],
    isForEjected: true,
  },
  {
    name: 'unused-files-webpack-plugin',
    types: [ProjectType.React],
    isForEjected: true,
  },
  {
    name: 'eslint-import-resolver-webpack',
    types: [ProjectType.React],
    isForEjected: true,
  },
  { name: 'eslint-config-react-app', types: [ProjectType.React] },
  { name: 'eslint-plugin-promise', types: [ProjectType.React] },
  { name: 'eslint-plugin-flowtype', types: [ProjectType.React] },
  {
    name: '@testing-library/react',
    version: 'latest',
    types: [ProjectType.React],
    children: [
      {
        name: '@testing-library/jest-dom',
        version: 'latest',
        types: [ProjectType.React],
      },
    ],
  },
  {
    name: '@moscord/common',
    version: 'latest',
    internal: true,
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
  },
  {
    name: '@moscord/styleguide',
    version: 'latest',
    internal: true,
    types: [ProjectType.React],
  },
  {
    name: 'prettier',
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
    children: [
      {
        name: 'eslint-config-prettier',
        types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
      },
    ],
  },
  {
    name: 'dotenv',
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
  },
  {
    name: 'cross-env',
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
  },
  {
    name: 'husky',
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
  },
  {
    name: 'lint-staged',
    types: [ProjectType.React, ProjectType.Node, ProjectType.Express],
  },
];
