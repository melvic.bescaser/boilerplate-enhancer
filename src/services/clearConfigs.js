import path from 'path';

import globby from 'globby';
import fse from 'fs-extra';
import colors from 'colors';

const clearConfigs = async (props) => {
  let globPaths = await globby(['*eslintrc*'], {
    dot: true,
    cwd: process.cwd(),
    gitignore: true,
  });

  if (globPaths.length === 0) {
    throw new Error(
      `ESLint configuration not detected, please run ${colors.green(
        'npx eslint --init',
      )} first.`,
    );
  }

  globPaths = [
    ...globPaths,
    ...(await globby(['*prettier*', '*babel*'], {
      dot: true,
      cwd: process.cwd(),
      gitignore: true,
    })),
  ];

  await Promise.all(
    globPaths.map((globPath) =>
      fse.remove(path.resolve(process.cwd(), globPath)),
    ),
  );

  return props;
};

export default clearConfigs;
