import path from 'path';

import fse from 'fs-extra';
import handlebars from 'handlebars';

import { AbsoluteImports } from '../config/constants';

const createWebpackConfig = async (props) => {
  const { answers } = props;
  const templateFile = (
    await fse.readFile(
      path.resolve(__dirname, '../templates/dynamic/react/webpack.config.hbs'),
    )
  ).toString();
  const template = handlebars.compile(templateFile);
  const webpackConfig = template({
    orgName: answers.orgName,
    projectName: answers.projectName,
    alias: Object.values(AbsoluteImports).map(
      (item) => `'${item.name}': path.resolve(__dirname, '${item.path}')`,
    ),
    ignores: [`'coverage/**/*'`, `'__mocks__/**/*'`, `'setupTests.js'`],
    externals: [
      `'react'`,
      /^@material-ui\/.+/,
      /^@apollo\/.+/,
      /^@moscord\/.+/,
    ],
  });

  await fse.outputFile(
    path.resolve(process.cwd(), 'webpack.config.js'),
    webpackConfig,
  );

  return props;
};

export default createWebpackConfig;
