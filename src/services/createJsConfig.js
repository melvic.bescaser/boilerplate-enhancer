import path from 'path';

import fse from 'fs-extra';

import { AbsoluteImports } from '../config/constants';

const createJsConfig = async (props) => {
  const { answers } = props;
  let jsConfig = {
    compilerOptions: {
      baseUrl: 'src',
    },
    include: ['src'],
  };

  if (!answers.isUsingReactScripts) {
    jsConfig = {
      compilerOptions: {
        target: 'es2017',
        allowSyntheticDefaultImports: false,
        baseUrl: './',
        paths: Object.values(AbsoluteImports).reduce(
          (prev, curr) => ({ ...prev, [`${curr.name}/*`]: [`${curr.path}/*`] }),
          {},
        ),
      },
      exclude: ['node_modules'],
    };
  }

  await fse.writeJson(path.resolve(process.cwd(), 'jsconfig.json'), jsConfig, {
    spaces: 2,
  });

  return props;
};

export default createJsConfig;
