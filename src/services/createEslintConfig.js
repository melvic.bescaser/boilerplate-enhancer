import path from 'path';

import fse from 'fs-extra';
import merge from 'lodash.merge';

const createEslintConfig = async (props) => {
  const eslintConfigDirectory = path.resolve(__dirname, '../templates/static');
  const eslintConfigs = await Promise.all([
    fse.readJson(path.resolve(eslintConfigDirectory, '.eslintrc.react.static')),
    fse.readJson(path.resolve(eslintConfigDirectory, '.eslintrc.base.static')),
  ]);
  const eslintConfig = merge(...eslintConfigs);

  await fse.writeJson(path.resolve(process.cwd(), '.eslintrc'), eslintConfig, {
    spaces: 2,
  });

  return props;
};

export default createEslintConfig;
