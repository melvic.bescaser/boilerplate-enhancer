import path from 'path';

import fse from 'fs-extra';

const createJestConfig = async (props) => {
  const { answers } = props;
  const jestConfigDirectory = path.resolve(__dirname, '../templates/static');
  let promises = [
    {
      source: path.resolve(jestConfigDirectory, 'jest.config.static'),
      target: path.resolve(process.cwd(), 'jest.config.js'),
    },
    {
      source: path.resolve(jestConfigDirectory, 'setupTests.static'),
      target: path.resolve(process.cwd(), 'src/setupTests.js'),
    },
  ].map(({ source, target }) => fse.copy(source, target));

  if (answers.isUsingReactScripts) {
    promises = [
      ...promises,
      fse.outputFile(
        path.resolve(process.cwd(), 'src/__mocks__/file.js'),
        `export default 'test-file-stub';`,
      ),
    ];
  }

  await Promise.all(promises);

  return props;
};

export default createJestConfig;
