import ora from 'ora';

import {
  ProjectType,
  Dependencies,
  DevDependencies,
} from '../config/constants';
import {
  flattenTree,
  execAsync,
  beautifyPackages,
  appendPackageVersion,
} from '../utils';

const installPackages = async (props) => {
  const { answers, options } = props;
  const spinner = ora();
  let dependencies = flattenTree({
    tree: Dependencies.filter(({ name }) => answers.packages.includes(name)),
    test: ({ types }) => types.includes(answers.projectType),
  });
  let devDependencies = flattenTree({
    tree: DevDependencies.filter(({ name, internal }) =>
      internal ? answers.packages.includes(name) : true,
    ),
    test: ({ types, isForEjected }) => {
      const isExist = types.includes(answers.projectType);

      if (!isExist) {
        return isExist;
      }

      if (answers.projectType !== ProjectType.React) {
        return isExist;
      }

      if (!(answers.isUsingReactScripts && isForEjected)) {
        return isExist;
      }

      return false;
    },
  });

  if (options.update) {
    dependencies = dependencies.map(({ name }) => name);
    devDependencies = devDependencies.map(({ name }) => name);
  } else {
    dependencies = dependencies.map(appendPackageVersion);
    devDependencies = devDependencies.map(appendPackageVersion);
  }

  if (dependencies.length > 0) {
    try {
      const beautifiedPackages = beautifyPackages({
        packages: dependencies,
      });
      spinner.start(`Installing ${beautifiedPackages}`);
      await execAsync({ command: `npm i ${dependencies.join(' ')}` });
      spinner.succeed(`Packages ${beautifiedPackages} are installed!`);
    } catch (error) {
      spinner.stop();
      throw error;
    }
  }

  if (devDependencies.length > 0) {
    try {
      const beautifiedPackages = beautifyPackages({
        packages: devDependencies,
      });
      spinner.start(`Installing ${beautifiedPackages}`);
      await execAsync({ command: `npm i -D ${devDependencies.join(' ')}` });
      spinner.succeed(`Packages ${beautifiedPackages} are installed!`);
    } catch (error) {
      spinner.stop();
      throw error;
    }
  }

  return props;
};

export default installPackages;
