import path from 'path';

import fse from 'fs-extra';

const addPackageJsonConfig = async (props) => {
  const packageJsonPath = path.resolve(process.cwd(), 'package.json');
  const huskyConfig = {
    hooks: {
      'pre-commit': 'lint-staged',
    },
  };
  const lintStagedConfig = {
    '*.{js,jsx}': [
      'prettier --write',
      'eslint --cache --fix',
      'cross-env BABEL_ENV=test jest --bail --findRelatedTests',
    ],
  };
  const packageJson = await fse.readJson(packageJsonPath);

  await fse.writeJson(
    packageJsonPath,
    {
      ...packageJson,
      husky: huskyConfig,
      'lint-staged': lintStagedConfig,
    },
    { spaces: 2 },
  );

  return props;
};

export default addPackageJsonConfig;
