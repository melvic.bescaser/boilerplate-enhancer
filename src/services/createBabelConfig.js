import path from 'path';

import fse from 'fs-extra';

import { AbsoluteImports } from '../config/constants';

const createBabelConfig = async (props) => {
  const babelConfig = await fse.readJson(
    path.resolve(__dirname, '../templates/static/.babelrc.react.static'),
  );
  const babelPluginModuleResolver = [
    'module-resolver',
    {
      root: ['./src'],
      alias: Object.values(AbsoluteImports).reduce(
        (prev, curr) => ({ ...prev, [curr.name]: `./${curr.path}` }),
        {},
      ),
    },
  ];

  await fse.writeJson(
    path.resolve(process.cwd(), '.babelrc'),
    {
      ...babelConfig,
      plugins: [...babelConfig.plugins, babelPluginModuleResolver],
    },
    {
      spaces: 2,
    },
  );

  return props;
};

export default createBabelConfig;
