import { Dependencies, DevDependencies } from '../config/constants';

const getPackages = ({ projectType }) => {
  const dependencies = Dependencies.filter(({ types }) =>
    types.includes(projectType),
  ).map(({ name }) => name);
  const devDependencies = DevDependencies.filter(
    ({ types, internal }) => internal && types.includes(projectType),
  ).map(({ name }) => name);

  return { dependencies, devDependencies };
};

export default getPackages;
