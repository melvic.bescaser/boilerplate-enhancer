import path from 'path';

import fse from 'fs-extra';

const setupRedux = async (props) => {
  const { answers } = props;

  if (!answers.packages.includes('redux')) {
    return props;
  }

  await fse.copy(
    path.resolve(__dirname, '../templates/scripts/react/ducks'),
    path.resolve(process.cwd(), 'src/ducks'),
  );

  return props;
};

export default setupRedux;
