import path from 'path';

import fse from 'fs-extra';

const createPrettierConfig = async (props) => {
  const prettierConfigDirectory = path.resolve(
    __dirname,
    '../templates/static',
  );

  await Promise.all(
    [
      {
        source: path.resolve(prettierConfigDirectory, '.prettierrc.static'),
        target: path.resolve(process.cwd(), '.prettierrc'),
      },
      {
        source: path.resolve(prettierConfigDirectory, '.prettierignore.static'),
        target: path.resolve(process.cwd(), '.prettierignore'),
      },
    ].map(({ source, target }) => fse.copy(source, target)),
  );

  return props;
};

export default createPrettierConfig;
